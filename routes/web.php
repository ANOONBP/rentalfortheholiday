<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Controller@home')->name('home');
Route::get('/login', 'Controller@login')->name('login');
Route::get('/logout', 'Controller@logout')->name('logout');
Route::get('/getCode', 'Controller@getCode')->name('getCode');
