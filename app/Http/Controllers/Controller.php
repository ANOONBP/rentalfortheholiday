<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
// use Illuminate\Http\Request;
use Session;
use Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        // User campaign middleware for this controller.
        $this->middleware('sso', ['except' => ['login', 'getCode'] ]);
    }

    function home(){

        $json = $this->curl(
        config('app.serverURL')."/user/info",
        json_encode(array(
            "Authorization" => Session::get('sso')->TokenType.' '.Session::get('sso')->AccessToken,
            "consumerKey" => config('app.consumerKey'),
            "consumerSecret" => config('app.consumerSecret')
        )));

        Session::put('info',  $json);
        
        return view('pages.home')->with('data', Session::get('sso'));
    }

    function login(){
        if(Session::has('sso')){
            
            // Return to next request
            return redirect()->route('home');
        }
        return view('pages.login');
    }

    function logout(){
        Session::remove('sso');
        
        return redirect()->route('login');
    }

    function getCode(){
        if(Request::get('code')){

            $json = $this->curl(
            config('app.serverURL')."/auth/accesstoken",
            json_encode(array(
                "code" => Request::get('code'),
                "consumerKey" => config('app.consumerKey'),
                "consumerSecret" => config('app.consumerSecret'),
                "redirectURL" => config('app.redirectURL')
            )));

            Session::put('sso',  $json);


            return redirect()->route('home');
        }
    }

    function curl($CURLOPT_URL, $CURLOPT_POSTFIELDS){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$CURLOPT_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $CURLOPT_POSTFIELDS);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);

        return json_decode($server_output);
    }
}
