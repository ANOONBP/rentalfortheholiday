<!DOCTYPE html>
<html lang="en">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="desrciption" content="">
        <meta name="author" content="">
        <title>Rental For Holidays</title>

        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <style>
            html,body{
                overflow: hidden;
                font-family: 'Montserrat', sans-serif;
                margin: 0;
               
            }

            html{
                background-color: rgba(0,0,0,0.05);
            }
            video{
                position: absolute;
                z-index: -1000;
            }

            nav{
                height: 160px;
                display: flex;
                align-items: center;
                background-color: white;
                box-shadow: 0 1px 1px 1px rgba(0,0,0,0.15);
            }

            nav > space{
                flex: 2;
            }

            nav > div{
                flex: 1;
                display: flex;
                justify-content: center;
            }

            nav > div > span, nav > div > a{
                font-size: 24px;
                color: black;
                text-decoration: none;
                cursor: pointer;
            }

            nav > div > span:hover hr, nav > div > a:hover hr{
                width: 100%;
                border-color: black;
            }

            main{
                display: block;
                width: 50vw;
                height: 50vh;
                margin-top: 20vh;
                margin-left: 25vw;
            }

            nav > .title{
                height: 100%;
                text-align: center;
                cursor: initial;
            }

            hr{
                margin: 2px 0;
                border: 1px solid white;
                width: 0;
                transition: width 0.25s;
            }

            /* home */
            page{
                width: 60vw;
                height: auto;
                display: flex;
                flex-direction: column;
                align-items: flex;
                margin: auto;
                padding-left: 25px;
                background-color: white;
            }

            title{
                display: block;
                margin: 30px 0;
                font-size: 32px;
                background-color: white;
            }

            page-body{
                width: 100%;
                height: auto;
                display: flex;
                flex-direction: column;
                
            }

            page-body row{
                width: 100%;
                display: flex;
                margin: 20px 0;
                font-size: 24px;
            }

            page-body label{
                width: auto;
                text-align: end;
            }

            page-body label::after{
                content: ' :';
            }

            page-body span{
                width: 50%;
            }

            page-body span::before{
                content: ' ';
            }

            header{
                font-size: 32px;
                height: 150px;
                width: 60vw;
                display: flex;
                align-items: center;
                padding-left: 25px;
                margin: 25px auto;
                background-color: white;
            }

        </style>

    </head>

    <body>
 
        <nav>
            <space></space>
            <div><span>Home<hr></span></div>
            <div><span>Production<hr></span></div>
            <div><span>Services<hr></span></div>
            <div class='title'><img  src='video/logo.jpg'></img></div>
            <div><span>About<hr></span></div>
            <div><span>Contract<hr></span></div>

            @if(Session::has('sso'))
                <div><a href="{{config('app.serverURL').'/auth/logout?key='.Session::get('sso')->AccessToken.'&redirectURL='.config('app.logoutURL')}}">Logout<hr></a></div>
            @else
                <div><a href="{{config('app.serverURL').'/auth/login?consumerKey='.config('app.consumerKey').'&redirectURL='.config('app.redirectURL')}}">Login<hr></a></div>
            @endif
            <space></space>
        </nav>

        @yield('content')
        <script>           
            var ssoLogin = document.getElementsByTagName('body')[0].appendChild(document.createElement('button'));
            ssoLogin.style.display = 'flex';
            ssoLogin.style.position = 'fixed';
            ssoLogin.style.height = '50px';
            ssoLogin.style.top = '10px';
            ssoLogin.style.right = '10px'
            ssoLogin.style.border = 'none';
            ssoLogin.style.backgroundColor = 'rgb(0,0,120)';
            ssoLogin.style.color = 'white';
            ssoLogin.style.fontSize = '18px';
            ssoLogin.style.padding = '0 30px';
            ssoLogin.style.cursor = 'pointer';
            ssoLogin.innerHTML = 'Login';
            ssoLogin.onclick = function(e){
                window.location = {!!json_encode(config('app.serverURL'))!!} + '/auth/login?consumerKey=' + {!!json_encode(config('app.consumerKey'))!!} + '&redirectURL=' + {!!json_encode(config('app.redirectURL'))!!};
            }
        </script>
    </body>
</html>
