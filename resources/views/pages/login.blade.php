@extends('layouts.app')

@section('content')

<video autoplay muted loop id="myVideo">
    <source src="video/login.mp4" type="video/mp4">
    Your browser does not support HTML5 video.
</video>

<script>
    var video = document.getElementsByTagName('video')[0];
    
    window.onload = function(){
        
        video.setAttribute('width', window.innerWidth);
    }

    window.onresize = function(){
        
        video.setAttribute('width', window.innerWidth);
    }
</script>

<main>
    <video autoplay muted loop id="myVideo">
        <source src="video/login2.mp4" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>
</main>

<script>
    var video = document.getElementsByTagName('video')[1];
    
    window.onload = function(){
        
        video.setAttribute('width', window.innerWidth / 2);
    }

    window.onresize = function(){
        
        video.setAttribute('width', window.innerWidth / 2);
    }
</script>
@endsection

